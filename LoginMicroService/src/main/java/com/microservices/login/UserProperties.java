package com.microservices.login;

public class UserProperties 
{
	public int role; //0 is when login fails,1 is for admin, 2 is for instructor, 3 is for student
	public String uni;
	public int loginStatus; //1 is set for success and 0 is set for failure
	
	public void setUserProperties(int role, String uni, int loginStatus)
	{
		this.role = role;
		this.uni = uni;
		this.loginStatus = loginStatus;
	}
}
