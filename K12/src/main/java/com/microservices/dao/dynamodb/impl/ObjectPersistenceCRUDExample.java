package com.microservices.dao.dynamodb.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.microservices.RecordItem;

public class ObjectPersistenceCRUDExample {
    
    static AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        
    public static void main(String[] args) throws IOException {
        testCRUDOperations();  
        System.out.println("Example complete!");
    }

   
        
    private static void testCRUDOperations() {

        RecordItem item = new RecordItem();
        item.setFirstName("Rohit");
        item.setId("rbg2134");
        item.setUniversity("NYU");
        
        // Save the item (book).
        client.setEndpoint("http://localhost:8000");
        DynamoDBMapper mapper = new DynamoDBMapper(client);
     //   mapper.save(item);
        
     /*   RecordItem item1 = new RecordItem();
        item.setId("rbg2135");
        // Retrieve the item.
        DynamoDBQueryExpression<RecordItem> queryExpression = new DynamoDBQueryExpression<RecordItem>()
                .withHashKeyValues(item1);
        
        List<RecordItem> itemRetrieved = mapper.query(RecordItem.class, queryExpression);
        for (RecordItem record : itemRetrieved) {
        	System.out.println(record);
        }
       */ 
        RecordItem record = mapper.load(RecordItem.class, "rbg2137","NYU");
        System.out.println(record);
        
        RecordItem item1 = new RecordItem();
        item1.setId("rbg2135");
        // Retrieve the item.
        DynamoDBQueryExpression<RecordItem> queryExpression = new DynamoDBQueryExpression<RecordItem>()
                .withHashKeyValues(item1);
        
        
        List<RecordItem> itemRetrieved = mapper.query(RecordItem.class, queryExpression);
        for (RecordItem rec : itemRetrieved) {
        	System.out.println(rec);
        }
        
      /*  // Update the item.
        itemRetrieved.setISBN("622-2222222222");
        itemRetrieved.setBookAuthors(new HashSet<String>(Arrays.asList("Author1", "Author3")));
        mapper.save(itemRetrieved);
        System.out.println("Item updated:");
        System.out.println(itemRetrieved);
        
        // Retrieve the updated item.
        DynamoDBMapperConfig config = new DynamoDBMapperConfig(DynamoDBMapperConfig.ConsistentReads.CONSISTENT);
        CatalogItem updatedItem = mapper.load(CatalogItem.class, 601, config);
        System.out.println("Retrieved the previously updated item:");
        System.out.println(updatedItem);
        
        // Delete the item.
        mapper.delete(updatedItem);
        
        // Try to retrieve deleted item.
        CatalogItem deletedItem = mapper.load(CatalogItem.class, updatedItem.getId(), config);
        if (deletedItem == null) {
            System.out.println("Done - Sample item is deleted.");
        }*/
    }
}

