package com.microservices;

import org.json.simple.JSONObject;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName="K12")
public class RecordItem {
    private String SID;
    private String University;
    private String FirstName;
    
    //Partition key
    @DynamoDBHashKey(attributeName="SID")
    public String getId() { return SID; }
    public void setId(String id) { this.SID = id; }
    
    @DynamoDBAttribute(attributeName="FirstName")
    public String getFirstName() { return FirstName; }    
    public void setFirstName(String title) { this.FirstName = title; }
    
    @DynamoDBRangeKey(attributeName="University")
    public String getUniversity() { return University; }    
    public void setUniversity(String univ) { this.University = univ;}
    
    
    @Override
    public String toString() {
        return "Student [SID=" + SID + ", University=" + University
        + ", FirstName=" + FirstName + "]";            
    }
    
    public JSONObject toJson(){
		JSONObject obj = new JSONObject();
		obj.put("SID", SID);
		obj.put("University", University);
		obj.put("FirstName", FirstName);
		/*
		for(String key : otherInfo.keySet()){
			obj.put(key, otherInfo.get(key));
		}*/
		return obj;
	}
}
