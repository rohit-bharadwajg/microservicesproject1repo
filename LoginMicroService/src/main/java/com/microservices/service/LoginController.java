package com.microservices.service;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.microservices.login.CrudObject;
import com.microservices.login.LoginHandler;
import com.microservices.login.UserProperties;


@RestController

public class LoginController 
{
	@RequestMapping(value = "/hello", method = RequestMethod.GET, produces = MediaType.ALL_VALUE)
	public String test()
	{
		return "Hello Login";
	}
	
	@RequestMapping(value = "/login/{uni}/{password}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public UserProperties login(@PathVariable(value="uni") String uni,@PathVariable(value="password") String password)
	{
		UserProperties returnObj = new UserProperties();
		returnObj = LoginHandler.getLoginDetails(uni, password);
		return returnObj;
	}
	
	@RequestMapping("/login/addNewUser")
	public CrudObject addNewUser(@RequestParam(value="uni") String uni,@RequestParam(value="password") String password,
			@RequestParam(value="role") int role)
	{
		CrudObject obj = new CrudObject();
		int status = 0;
		String returnString = "Adding a new user failed";
		//if(adminRole == 1)
		//{
			int result = 0;
			result = LoginHandler.addNewUser(uni, password, role);
			if(result == 1)
			{
				returnString = "Adding a new user succeeded";
				status = 1;
			}
		//}
		//else
		//{
			//returnString = "You do not have relevant permissions to add a new user";
		//}
		obj.setCrudProperties(returnString, status);
		return obj;
	}
	
	@RequestMapping("/login/deleteUser")
	public CrudObject deleteUser(@RequestParam(value="uni") String uni)
	{
		CrudObject obj = new CrudObject();
		int status = 0;
		String returnString = "Deleting the user failed";
		//if(adminRole == 1)
		//{
			int result = 0;
			result = LoginHandler.deleteUser(uni);
			if(result == 1)
			{
				returnString = "Deleting the user succeeded";
				status = 1;
			}
		//}
		//else
		//{
			//returnString = "You do not have relevant permission to delete the user";
		//}
		obj.setCrudProperties(returnString, status);
		return obj;
	}
	
	@RequestMapping("/login/updateUser")
	public CrudObject updateUser(@RequestParam(value="uni") String uni,@RequestParam(value="password") String password,
			@RequestParam(value="role") int role)
	{
		CrudObject obj = new CrudObject();
		int status = 0;
		String returnString = "Updating the user failed";
		//if(adminRole == 1)
		//{
			int result = 0;
			result = LoginHandler.updateLoginDetails(uni, password, role);
			if(result == 1)
			{
				status = 1;
				returnString = "Updating the user succeeded";
			}
		//}
		//else
		//{
			//returnString = "You do not have relevant permissions to update the user";
		//}
		obj.setCrudProperties(returnString, status);
		return obj;
	}
	

}
