package com.microservices.utils;
 
import java.io.IOException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microservices.messaging.RegistrationObject;
 
public class CreateJson {

	public static String getJsonString(RegistrationObject obj){
		ObjectMapper mapper = new ObjectMapper();
	//	mapper.setVisibility(JsonMethod.FIELD, Visibility.ANY);
		try {
			return mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
    public static void main(String[] args) throws IOException {
 
       RegistrationObject obj = new RegistrationObject("addition", "cid", "uni");
       System.out.println(getJsonString(obj));
 
    }
 
}