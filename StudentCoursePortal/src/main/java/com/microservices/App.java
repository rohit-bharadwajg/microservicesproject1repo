package com.microservices;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.microservices.dao.JDBCRecordDAO;

public class App {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
	
			ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
//			EmployeeDAO employeeDAO = (EmployeeDAO) context.getBean("employeeDAO");
//	        Employee employee1 = new Employee(123, "javacodegeeks", 30);
//	        employeeDAO.insert(employee1);
//	        Employee employee2 = employeeDAO.findById(123);
//	        System.out.println(employee2);	
//	        
	        JDBCRecordDAO jdbcRecordDAO = (JDBCRecordDAO) context.getBean("jdbcRecordDAO");
//	        Employee employee3 = new Employee(456, "javacodegeeks", 34);
//	        jdbcEmployeeDAO.insert(employee3);
	 
//	        Employee employee4 = jdbcEmployeeDAO.findById(456);
//	        System.out.println(employee4);	
	        
//	        List<Employee> employees = jdbcEmployeeDAO.findAll();
//	        System.out.println(employees);	
	        
	        
//	        String name = jdbcEmployeeDAO.findNameById(456);
//	        System.out.println(name);	
	        
	        
	        StudentRecord rec1 = new StudentRecord(213, "rbg2134", "ee5669");
	        StudentRecord rec2 = new StudentRecord(233, "rbg2134", "ee5660");
	        List<StudentRecord> records = new ArrayList();
	        records.add(rec1);
	        records.add(rec2);
	        jdbcRecordDAO.insertBatch1(records);
	        System.out.println(" inserted rows: " + records);

	        System.out.println(" FindAll : " + jdbcRecordDAO.findAll());
	        jdbcRecordDAO.insertBatch2("UPDATE records SET uni ='Mary'");
	        
	        List<StudentRecord> employees = jdbcRecordDAO.findAll();
	        System.out.println("Updated column name of table: " + employees);	
	        
	        System.out.println(" FindAll : " + jdbcRecordDAO.findAll());
			context.close();
	}
}
