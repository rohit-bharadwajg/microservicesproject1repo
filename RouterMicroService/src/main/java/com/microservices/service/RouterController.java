package com.microservices.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

@RequestMapping("/studentPortal")
@RestController
public class RouterController {
	static Boolean loginSession = false;

	@RequestMapping(value = "/routerConfig", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public static String configRouter(@RequestBody String jsonStr) {
		String result = "Router Configuration Succeeded";
		JSONParser parser = new JSONParser();
		JSONObject json;
		String server = null;
		String service = null;
		String port = null;
		MongoClient mongoClient;
		try {
			mongoClient = new MongoClient("localhost", 27017);
			DB db = mongoClient.getDB("RouterDB");
			DBCollection dbCollection = db.getCollection("routerConfig");
			BasicDBObject query = new BasicDBObject();
			json = (JSONObject) parser.parse(jsonStr);
			service = (String) json.get("service");
			server = (String) json.get("server");
			port = (String) json.get("port");
			dbCollection.update(new BasicDBObject("functionName", service),
					new BasicDBObject("$set", new BasicDBObject("server", server)));
			dbCollection.update(new BasicDBObject("functionName", service),
					new BasicDBObject("$set", new BasicDBObject("port", port)));
		} catch (Exception e) {
			System.out.println(e.getMessage());
			result = e.getMessage();
		}
		return result;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public static String checkLogin(@RequestBody String jsonStr) {
		String result = "User is not valid";
		JSONParser parser = new JSONParser();
		JSONObject json;
		String uni = null;
		String password = null;
		String url = "http://localhost:9002/login/";
		try {
			json = (JSONObject) parser.parse(jsonStr);
			uni = (String) json.get("uni");
			password = (String) json.get("password");
			url = url + uni + "/" + password;
			RestTemplate restTemplate = new RestTemplate();
			String resultRest = restTemplate.getForObject(url, String.class);
			JSONObject loginStatusObj = (JSONObject) parser.parse(resultRest);
			String loginStatus = String.valueOf(loginStatusObj.get("loginStatus"));
			if (loginStatus.equals("1")) {
				result = resultRest;
				loginSession = true;
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return result;
	}

	@RequestMapping(value = "/login/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public static String addUser(@RequestBody String jsonStr) {
		String result = "User is not valid";
		JSONParser parser = new JSONParser();
		JSONObject json;
		String uni = null;
		String password = null;
		String role = null;
		String url = "http://localhost:9002/login/addNewUser?";
		try {
			json = (JSONObject) parser.parse(jsonStr);
			uni = (String) json.get("uni");
			password = (String) json.get("password");
			role = (String) json.get("role");
			url = url + "uni=" + uni + "&password=" + password + "&role=" + role;
			RestTemplate restTemplate = new RestTemplate();
			result = restTemplate.getForObject(url, String.class);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return result;
	}

	@RequestMapping(value = "/login/{uni}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public static String deleteUser(@PathVariable(value = "uni") String uni) {
		String url = "http://localhost:9002/login/deleteUser?uni=" + uni;
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(url, String.class);
	}

	@RequestMapping(value = "/login/{uni}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public static String updateUser(@RequestBody String jsonStr) {
		JSONParser parser = new JSONParser();
		JSONObject json;
		String uni = null;
		String password = null;
		String role = null;
		String url = "http://localhost:9002/login/updateUser?";
		String result = "UpdationFailed";
		try {
			json = (JSONObject) parser.parse(jsonStr);
			uni = (String) json.get("uni");
			password = (String) json.get("password");
			role = (String) json.get("role");
			url = url + "uni=" + uni + "&password=" + password + "&role=" + role;
			RestTemplate restTemplate = new RestTemplate();
			result = restTemplate.getForObject(url, String.class);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return result;
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test() {
		String s = "Hello World";
		return s;
	}

	// Registration calls

	@RequestMapping(value = "/registration", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public static ResponseEntity<String> getAllRegistrationRecords() {
		String url = "http://localhost:8080/registration/all";
		RestTemplate restTemplate = new RestTemplate();
		String toReturn = restTemplate.getForObject(url, String.class);
		return new ResponseEntity<String>(toReturn, HttpStatus.OK);
	}

	@RequestMapping(value = "/registration/column/{colName}", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
	public static ResponseEntity<String> addColumn(@PathVariable(value = "colName") String colName) {
		if (colName != null) {
			String url = "http://localhost:8080/registration/column/" + colName;
			RestTemplate restTemplate = new RestTemplate();
			return new ResponseEntity<String>(restTemplate.postForObject(url, null, String.class),HttpStatus.OK);
		}
		return new ResponseEntity<String>("Insufficient Data",HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/registration/{uni}/{cid}", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public static ResponseEntity<String> getRegistrationRecord(@PathVariable(value = "uni") String uni,
			@PathVariable(value = "cid") String cid) {
		if (uni != null && cid != null) {
			String url = "http://localhost:8080/registration/" + uni + "/" + cid;
			RestTemplate restTemplate = new RestTemplate();
			return new ResponseEntity<String>(restTemplate.getForObject(url, String.class),HttpStatus.OK);
		}
		return new ResponseEntity<String>("Insufficient Data",HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/registration/student/{uni}", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public static ResponseEntity<String> getRecordsByUni(@PathVariable(value = "uni") String uni) {
		if (uni != null) {
			String url = "http://localhost:8080/registration/student/" + uni;
			RestTemplate restTemplate = new RestTemplate();
			return new ResponseEntity<String>(restTemplate.getForObject(url, String.class),HttpStatus.OK);
		}
		return new ResponseEntity<String>("Insufficient Data",HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/registration/course/{cid}", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public static ResponseEntity<String> getRecordsByCid(@PathVariable(value = "cid") String cid) {
		if (cid != null) {
			String url = "http://localhost:8080/registration/course/" + cid;
			RestTemplate restTemplate = new RestTemplate();
			return new ResponseEntity<String>(restTemplate.getForObject(url, String.class),HttpStatus.OK);
		}
		return new ResponseEntity<String>("Insufficient Data",HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/registration/", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public static  ResponseEntity<String>  addRecord(@RequestBody String jsonStr) {
		if (jsonStr != null) {
			String result = "Insufficient Data";
			String url = "http://localhost:8080/registration/";
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			HttpEntity<String> entity = new HttpEntity<String>(jsonStr, headers);

			RestTemplate restTemplate = new RestTemplate();
			result = restTemplate.postForObject(url, entity, String.class);
			return new ResponseEntity<String>(result,HttpStatus.OK);
		}
		return new ResponseEntity<String>("Insufficient Data",HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/registration/{uni}/{cid}", method = RequestMethod.DELETE, produces = MediaType.TEXT_PLAIN_VALUE)
	public static ResponseEntity<String> deleteRecord(@PathVariable(value = "uni") String uni,
			@PathVariable(value = "cid") String cid) {
		if (uni != null && cid != null) {
			String url = "http://localhost:8080/registration/" + uni + "/" + cid;
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> result = restTemplate.getForEntity(url, String.class);
			System.out.println(result.getBody());
			if (result == null || result.getBody() == null || result.getBody().length() == 0
					|| result.getBody().equals("No records exist")) {
				return new ResponseEntity<String>("Cannot Delete. Record doesn't exist",HttpStatus.BAD_REQUEST);
			} else {
				restTemplate.delete(url, String.class);
				return new ResponseEntity<String>("Deleted Successfully",HttpStatus.OK);
			}
		}
		return new ResponseEntity<String>("Insufficient Data",HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/registration/column/{colName}", method = RequestMethod.DELETE, produces = MediaType.TEXT_PLAIN_VALUE)
	public static ResponseEntity<String> dropColumn(@PathVariable(value = "colName") String colName) {
		if (colName != null) {
			String url = "http://localhost:8080/registration/column/" + colName;
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.delete(url, String.class);
			String url2 = "http://localhost:8080/registration/columns";
			return new ResponseEntity<String>("Remaining Columns -> "+restTemplate.getForObject(url2, String.class), HttpStatus.OK);
		}
		return new ResponseEntity<String>("Insufficient Data",HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping(value = "/registration/student/{uni}", method = RequestMethod.DELETE, produces = MediaType.TEXT_PLAIN_VALUE)
	public static ResponseEntity<String> deleteRecordsByUni(@PathVariable(value = "uni") String uni) {
		if (uni != null) {
			String url = "http://localhost:8080/registration/student/" + uni;
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> result = restTemplate.getForEntity(url, String.class);
			System.out.println(result.getBody());
			if (result == null || result.getBody() == null || result.getBody().length() == 0
					|| result.getBody().equals("No records exist")) {
				return new ResponseEntity<String>("Cannot Delete. Record doesn't exist",HttpStatus.BAD_REQUEST);
			} else {
				restTemplate.delete(url, String.class);
				return new ResponseEntity<String>("Deleted Successfully",HttpStatus.OK);
			}
		}
		return new ResponseEntity<String>("Insufficient Data",HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/registration/course/{cid}", method = RequestMethod.DELETE, produces = MediaType.TEXT_PLAIN_VALUE)
	public static ResponseEntity<String> deleteRecordsByCid(@PathVariable(value = "cid") String cid) {
		if (cid != null) {
			String url = "http://localhost:8080/registration/course/" + cid;
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> result = restTemplate.getForEntity(url, String.class);
			System.out.println(result.getBody());
			if (result == null || result.getBody() == null || result.getBody().length() == 0
					|| result.getBody().equals("No records exist")) {
				return new ResponseEntity<String>("Cannot Delete. Record doesn't exist",HttpStatus.BAD_REQUEST);
			} else {
				restTemplate.delete(url, String.class);
				return new ResponseEntity<String>("Deleted Successfully",HttpStatus.OK);
			}
		}
		return new ResponseEntity<String>("Insufficient Data",HttpStatus.BAD_REQUEST);
	}

	// Student Micro Service calls
	@RequestMapping(value = "/student", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public static String getAllStudents() {
		String url1 = "http://localhost:3000/students/studentlist";
		String url2 = "http://localhost:3003/students/studentlist";
		String url3 = "http://localhost:3002/students/studentlist";
		RestTemplate restTemplate = new RestTemplate();
		String result1 = restTemplate.getForObject(url1, String.class);
		String result3 = restTemplate.getForObject(url3, String.class);
		String result2 = restTemplate.getForObject(url2, String.class);
		return "[" + result1 + "," + result2 + "," + result3 + "]";
	}

	@RequestMapping(value = "/student/{uni}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public static String getAllStudents(@PathVariable(value = "uni") String uni) {
		String url1 = "http://localhost:3000/students/student/" + uni;
		String url2 = "http://localhost:3003/students/student/" + uni;
		String url3 = "http://localhost:3002/students/student/" + uni;
		String firstCharacter = uni.substring(0, 1);
		String url = null;
		if (firstCharacter.matches("[a-k]")) {
			url = url1;
		} else if (firstCharacter.matches("[l-q]")) {
			url = url2;
		} else {
			url = url3;
		}
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(url, String.class);
	}

	@RequestMapping(value = "/student/{uni}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public static String addStudent(@RequestBody String body, @PathVariable(value = "uni") String uni) {
		String url1 = "http://localhost:3000/students/addstudent";
		String url2 = "http://localhost:3003/students/addstudent";
		String url3 = "http://localhost:3002/students/addstudent";
		String firstCharacter = uni.substring(0, 1);
		String url = null;
		if (firstCharacter.matches("[a-k]")) {
			url = url1;
		} else if (firstCharacter.matches("[l-q]")) {
			url = url2;
		} else {
			url = url3;
		}
		// url = "http://localhost:3000/students/addstudent";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<String> entity = new HttpEntity<String>(body, headers);
		String result = restTemplate.postForObject(url, entity, String.class);
		return result;
	}

	@RequestMapping(value = "/student/{uni}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public static String updateStudent(@RequestBody String body, @PathVariable(value = "uni") String uni) {
		String url1 = "http://localhost:3000/students/edit/" + uni;
		String url2 = "http://localhost:3003/students/edit/" + uni;
		String url3 = "http://localhost:3002/students/edit/" + uni;
		String firstCharacter = uni.substring(0, 1);
		String url = null;
		int set = 0;
		if (firstCharacter.matches("[a-k]")) {
			url = url1;
			set = 1;
		} else if (firstCharacter.matches("[l-q]")) {
			url = url2;
			set = 2;
		} else {
			url = url3;
			set = 3;
		}

		// String url = "http://localhost:3000/students/edit/" + uni;
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<String> entity = new HttpEntity<String>(body, headers);
		restTemplate.put(url, entity, uni);
		if (set == 1) {
			url = "http://localhost:3000/students/student/" + uni;
		} else if (set == 2) {
			url = "http://localhost:3003/students/student/" + uni;
		} else {
			url = "http://localhost:3002/students/student/" + uni;
		}
		// ResponseEntity<String> exchange = restTemplate.exchange(url,
		// HttpMethod.PUT, entity, String.class);
		// url = "http://localhost:3000/students/student/" + uni;
		// restTemplate = new RestTemplate();
		return restTemplate.getForObject(url, String.class);
	}

	@RequestMapping(value = "/student/{uni}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public static String deleteStudent(@PathVariable(value = "uni") String uni) {
		String url1 = "http://localhost:3000/students/deletestudent/" + uni;
		String url2 = "http://localhost:3003/students/deletestudent/" + uni;
		String url3 = "http://localhost:3002/students/deletestudent/" + uni;
		String firstCharacter = uni.substring(0, 1);
		String url = null;
		if (firstCharacter.matches("[a-k]")) {
			url = url1;
		} else if (firstCharacter.matches("[l-q]")) {
			url = url2;
		} else {
			url = url3;
		}
		// String url = "http://localhost:3000/students/deletestudent/"+uni;
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.delete(url, uni);
		// url = "http://localhost:3000/students/studentlist";
		url1 = "http://localhost:3000/students/studentlist";
		url2 = "http://localhost:3003/students/studentlist";
		url3 = "http://localhost:3002/students/studentlist";
		restTemplate = new RestTemplate();
		String result1 = restTemplate.getForObject(url1, String.class);
		String result2 = restTemplate.getForObject(url2, String.class);
		String result3 = restTemplate.getForObject(url3, String.class);
		return "[" + result1 + "," + result2 + "," + result3 + "]";
	}
	
	@RequestMapping(value = "/student/modifySchema", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public static String addColumnToStudent(@RequestBody String body)
	{
		String url1 = "http://localhost:3000/students/addcolumn";
		String url2 = "http://localhost:3003/students/addcolumn";
		String url3 = "http://localhost:3002/students/addcolumn";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(body, headers);
		String result1 = restTemplate.postForObject(url1, entity, String.class);
		String result2 = restTemplate.postForObject(url2, entity, String.class);
		String result3 = restTemplate.postForObject(url3, entity, String.class);
		return "[" + result1 + "," + result2 + "," + result3 + "]";
	}
	
	@RequestMapping(value = "/student/modifySchema/{columnName}", method = RequestMethod.DELETE, produces = MediaType.TEXT_PLAIN_VALUE)
	public static void deleteColumnFromStudent(@PathVariable(value = "columnName") String columnName)
	{

		String url1 = "http://localhost:3000/students/dropcolumn/" + columnName;
		String url2 = "http://localhost:3003/students/dropcolumn/" + columnName;
		String url3 = "http://localhost:3002/students/dropcolumn/" + columnName;
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.delete(url1, columnName);
		restTemplate.delete(url2, columnName);
		restTemplate.delete(url3, columnName);
	}
	

	// Make course calls
	@RequestMapping(value = "/course", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public static String getCourses()
	{
		String url = "http://localhost:9000/api/getCourses";
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(url, String.class);
	}

	@RequestMapping(value = "/course/{cid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public static String getCourseById(@PathVariable(value = "cid") String cid)
	{
		String url = "http://localhost:9000/api/getCourses/" + cid;
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(url, String.class);
	}
	
	@RequestMapping(value = "/course/{cid}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public static String updateCourse(@PathVariable(value = "cid") String cid, @RequestBody String body)
	{
		String url = "http://localhost:9000/api/updateCourse/" + cid;
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<String> entity = new HttpEntity<String>(body ,headers);
		restTemplate.put(url, entity ,cid);
		url = "http://localhost:9000/api/getCourses/" + cid;
		return restTemplate.getForObject(url, String.class);
	}
	@RequestMapping(value = "/course/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public static String createCourse(@RequestBody String body)
	{
		String url = "http://localhost:9000/api/createCourse";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<String> entity = new HttpEntity<String>(body ,headers);
		String result = restTemplate.postForObject(url, entity, String.class);
		return result;
	}
	
	@RequestMapping(value = "/course/{cid}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public static String deleteCourse(@PathVariable(value = "cid") String cid)
	{
		String url = "http://localhost:9000/api/deleteCourse/" + cid;
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.delete(url, cid);
		url = "http://localhost:9000/api/getCourses";
		return restTemplate.getForObject(url, String.class);
		
	}
	
	@RequestMapping(value = "/course/modifySchema", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public static String addColumnToCourse(@RequestBody String body)
	{
		String url = "http://localhost:9000/addcolumn";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(body ,headers);
		String result = restTemplate.postForObject(url, entity, String.class);
		return result;
	}
	
	@RequestMapping(value = "/course/modifySchema/{columnName}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public static void deleteColumnFromCourse(@PathVariable(value = "columnName") String columnName)
	{
		String url = "http://localhost:9000/dropcolumn/" + columnName;
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.delete(url, columnName);
	}

}
