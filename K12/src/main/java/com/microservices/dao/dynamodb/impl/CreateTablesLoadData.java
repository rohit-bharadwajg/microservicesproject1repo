package com.microservices.dao.dynamodb.impl;
//Copyright 2012-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
//Licensed under the Apache License, Version 2.0.

import java.util.ArrayList;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.LocalSecondaryIndex;
import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;

public class CreateTablesLoadData {

// static DynamoDB dynamoDB = new DynamoDB(new AmazonDynamoDBClient(
//         new ProfileCredentialsProvider()));
	static AmazonDynamoDBClient client = new AmazonDynamoDBClient(new ProfileCredentialsProvider());
	static DynamoDB dynamoDB = new DynamoDB(client);
	
	;

 static String k12TableName = "K12";

 public static void main(String[] args) throws Exception {

     try {
    	 client.setEndpoint("http://localhost:8000"); 
         deleteTable(k12TableName);

         // Parameter1: table name // Parameter2: reads per second //
         // Parameter3: writes per second // Parameter4/5: partition key and data type
         // Parameter6/7: sort key and data type (if applicable)

         createTable(k12TableName, 10L, 5L, "SID", "S", "University", "S");
         
         loadSampleStudents(k12TableName);

     } catch (Exception e) {
         System.err.println("Program failed:");
         System.err.println(e.getMessage());
     }
     System.out.println("Success.");
 }

 private static void deleteTable(String tableName) {
     Table table = dynamoDB.getTable(tableName);
     try {
         System.out.println("Issuing DeleteTable request for " + tableName);
         table.delete();
         System.out.println("Waiting for " + tableName
             + " to be deleted...this may take a while...");
         table.waitForDelete();

     } catch (Exception e) {
         System.err.println("DeleteTable request failed for " + tableName);
         System.err.println(e.getMessage());
     }
 }

 private static void createTable(
     String tableName, long readCapacityUnits, long writeCapacityUnits, 
     String partitionKeyName, String partitionKeyType, 
     String sortKeyName, String sortKeyType) {

     try {

         ArrayList<KeySchemaElement> keySchema = new ArrayList<KeySchemaElement>();
         keySchema.add(new KeySchemaElement()
             .withAttributeName(partitionKeyName)
             .withKeyType(KeyType.HASH)); //Partition key
         
         ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();
         attributeDefinitions.add(new AttributeDefinition()
             .withAttributeName(partitionKeyName)
             .withAttributeType(partitionKeyType));

         if (sortKeyName != null) {
             keySchema.add(new KeySchemaElement()
                 .withAttributeName(sortKeyName)
                 .withKeyType(KeyType.RANGE)); //Sort key
             attributeDefinitions.add(new AttributeDefinition()
                 .withAttributeName(sortKeyName)
                 .withAttributeType(sortKeyType));
         }

         CreateTableRequest request = new CreateTableRequest()
                 .withTableName(tableName)
                 .withKeySchema(keySchema)
                 .withProvisionedThroughput( new ProvisionedThroughput()
                     .withReadCapacityUnits(readCapacityUnits)
                     .withWriteCapacityUnits(writeCapacityUnits));

       
         if (k12TableName.equals(tableName)) {
             
             attributeDefinitions.add(new AttributeDefinition()
                 .withAttributeName("FirstName")
                 .withAttributeType("S"));

             ArrayList<LocalSecondaryIndex> localSecondaryIndexes = new ArrayList<LocalSecondaryIndex>();
             localSecondaryIndexes.add(new LocalSecondaryIndex()
                 .withIndexName("FirstName-Index")
                 .withKeySchema(
                     new KeySchemaElement().withAttributeName(partitionKeyName).withKeyType(KeyType.HASH),  //Partition key
                     new KeySchemaElement() .withAttributeName("FirstName") .withKeyType(KeyType.RANGE))  //Sort key
                 .withProjection(new Projection() .withProjectionType(ProjectionType.KEYS_ONLY)));

             request.setLocalSecondaryIndexes(localSecondaryIndexes);
         }

         request.setAttributeDefinitions(attributeDefinitions);

         System.out.println("Issuing CreateTable request for " + tableName);
         Table table = dynamoDB.createTable(request);
         System.out.println("Waiting for " + tableName
             + " to be created...this may take a while...");
         table.waitForActive();

     } catch (Exception e) {
         System.err.println("CreateTable request failed for " + tableName);
         System.err.println(e.getMessage());
     }
 }

 
 private static void loadSampleStudents(String tableName) {
     try {
       
         Table table = dynamoDB.getTable(tableName);

         System.out.println("Adding data to " + tableName);

         // Add threads.

         Item item = new Item()
             .withPrimaryKey("SID", "rbg2134")
             .withString("Course", "MS in DataScience")
             .withString("FirstName", "Gernapudi")
             .withString("University", "Columbia");
         table.putItem(item);


         item = new Item()
             .withPrimaryKey("SID", "rbg2135")
             .withString("Course", "MS in ComputerScience")
             .withString("FirstName", "Bharadwaj")
             .withString("University", "Columbia");
         table.putItem(item);
         

         item = new Item()
             .withPrimaryKey("SID", "rbg2136")
             .withString("Course", "MS in DataScience")
             .withString("FirstName", "Rohit")
             .withString("University", "Columbia");
         table.putItem(item);
         

         item = new Item()
             .withPrimaryKey("SID", "rbg2137")
             .withString("Course", "MS in DataScience")
             .withString("FirstName", "Gernapudi")
             .withString("University", "NYU");
         table.putItem(item);
         

         item = new Item()
             .withPrimaryKey("SID", "rbg2138")
             .withString("Course", "MS in DataScience")
             .withString("FirstName", "Gernapudi")
             .withString("University", "Columbia");
         table.putItem(item);
         
         
     } catch (Exception e) {
         System.err.println("Failed to create item in " + tableName);
         System.err.println(e.getMessage());

     }
 }

}
