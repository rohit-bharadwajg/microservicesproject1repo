package com.microservices.login;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class LoginHandler 
{
	public static UserProperties getLoginDetails(String uni, String password)
	{
		int status;
		UserProperties returnObj = new UserProperties();
		status = retrieveAndComparePassword(uni,password);
		if(status == 1)
		{
			returnObj.setUserProperties(1, uni , 1);
		}
		else
		{
			returnObj.setUserProperties(0, uni , 0);
		}
		return returnObj;
	}
	
	public static int addNewUser(String uni, String password, int role)
	{
		int status =0;
		MongoClient mongoClient;
		try 
		{
			mongoClient = new MongoClient("localhost",27017);
			DB db = mongoClient.getDB("LoginDB");
			DBCollection dbCollection =  db.getCollection("db");
			BasicDBObject basicDBObject = new BasicDBObject();
			ObjectId id = new ObjectId();
            basicDBObject.put("_id", id);
            basicDBObject.put("uni",uni);
            basicDBObject.put("password",password);
            basicDBObject.put("role",role);
            dbCollection.insert(basicDBObject);
            status = 1;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
        return status;    
	}
	
	public static int updateLoginDetails(String uni, String password, int role)
	{
		int status = 0;
		MongoClient mongoClient;
		try
		{
			mongoClient = new MongoClient("localhost",27017);
			DB db = mongoClient.getDB("LoginDB");
			DBCollection dbCollection =  db.getCollection("db");
			BasicDBObject query = new BasicDBObject();
			dbCollection.update(new BasicDBObject("uni", uni), new BasicDBObject("$set", new BasicDBObject("password", password)));
			dbCollection.update(new BasicDBObject("uni", uni), new BasicDBObject("$set", new BasicDBObject("role", role)));
			status = 1;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return status;
	}
	
	public static int deleteUser(String uni)
	{
		int status = 0;
		MongoClient mongoClient;
		try
		{
			mongoClient = new MongoClient("localhost",27017);
			DB db = mongoClient.getDB("LoginDB");
			DBCollection dbCollection =  db.getCollection("db");
			BasicDBObject query = new BasicDBObject();
			query.put("uni",new BasicDBObject("$eq",uni));
			dbCollection.remove(query);
			status = 1;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return status;
	}
	
	public static int retrieveAndComparePassword(String uni, String password)
	{
		int status = 0;
		MongoClient mongoClient;
		try
		{
			mongoClient = new MongoClient("localhost",27017);
			DB db = mongoClient.getDB("LoginDB");
			DBCollection dbCollection =  db.getCollection("db");
			BasicDBObject basicDBObject = new BasicDBObject();
	        basicDBObject.get("_id");
	        DBCursor dbCursor = dbCollection.find(basicDBObject);
	        DBObject temp;
	        while(dbCursor.hasNext())
	         {
	        	temp = dbCursor.next();
	        	String universityId = (String) temp.get("uni");
	        	String passwrd = (String) temp.get("password");
	        	if(universityId.equalsIgnoreCase(uni))
	        	{
	        		if(password.equals(passwrd))
	        		{
	        			status = 1;
	        			break;
	        		}
	        	}
	         }
	      }
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return status;
	}

}
