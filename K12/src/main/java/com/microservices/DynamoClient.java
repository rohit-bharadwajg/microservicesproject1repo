package com.microservices;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

public class DynamoClient {
	static AmazonDynamoDBClient client = new AmazonDynamoDBClient();
	static DynamoDBMapper mapper = new DynamoDBMapper(client);
    
    public static DynamoDBMapper getMapper(){
    	client.setEndpoint("http://localhost:8000");
    	return mapper;
    }
}
