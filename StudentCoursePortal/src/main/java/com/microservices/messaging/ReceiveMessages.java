package com.microservices.messaging;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ReceiveMessages {
	private static final String EXCHANGE_NAME = "logs";
	private static Connection connection;
	private static HandleMessages hm;

	private static void init() throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		connection = factory.newConnection();
		hm = new HandleMessages();
	}

	public static Channel getChannel(String exchangeName, String queueName) throws IOException, TimeoutException {
		if (connection == null) {
			init();
		}
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(exchangeName, "fanout");
		channel.queueDeclare(queueName,false,false,false,null);
		channel.queueBind(queueName, exchangeName, "");
		return channel;
	}

	public static Consumer getConsumer(Channel channel) {
		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				hm.printAppend(message);
				System.out.println(" [x] Received '" + message + "'");
			}
		};
		return consumer;
	}

	public static void main(String[] argv) throws Exception {
	
/*		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
		// String queueName = channel.queueDeclare().getQueue();
		
		String queueName = "registration";
		System.out.println(queueName);
		channel.queueBind(queueName, EXCHANGE_NAME, "");
*/
		Channel channel = getChannel("logs","registration");
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		Consumer consumer = getConsumer(channel);
		channel.basicConsume("registration", true, consumer);

	}
}
