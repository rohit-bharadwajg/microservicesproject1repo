package com.microservices.service;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.microservices.DynamoClient;
import com.microservices.RecordItem;

@RequestMapping("/k12")
@RestController
public class RegistrationController {

	@RequestMapping(value = "/student/{sid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getStudent(@PathVariable(value = "sid") String sid) {
		RecordItem item1 = new RecordItem();
		item1.setId(sid);
		// Retrieve the item.
		DynamoDBQueryExpression<RecordItem> queryExpression = new DynamoDBQueryExpression<RecordItem>()
				.withHashKeyValues(item1);
		DynamoDBMapper mapper = DynamoClient.getMapper();
		List<RecordItem> itemRetrieved = mapper.query(RecordItem.class, queryExpression);
		return convertListToString(itemRetrieved);
	}
	
	@RequestMapping(value = "/university/{univ}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getUniv(@PathVariable(value = "univ") String univ) {
		RecordItem item1 = new RecordItem();
		item1.setUniversity(univ);
		// Retrieve the item.
		DynamoDBQueryExpression<RecordItem> queryExpression = new DynamoDBQueryExpression<RecordItem>()
				.withHashKeyValues(item1);
		DynamoDBMapper mapper = DynamoClient.getMapper();
		List<RecordItem> itemRetrieved = mapper.query(RecordItem.class, queryExpression);
		return convertListToString(itemRetrieved);
	}

	
	@RequestMapping(value = "/student/{univ}/{sid}/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getRecord(@PathVariable(value = "sid") String sid, @PathVariable(value = "univ") String univ) {
		// Save the item (book).
		DynamoDBMapper mapper = DynamoClient.getMapper();
		RecordItem record = mapper.load(RecordItem.class, sid, univ);
		System.out.println(record);
		return record.toJson().toJSONString();
	}

	@RequestMapping(value = "/student/{sid}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String deleteRecordBySID(@PathVariable(value = "sid") String sid) {

		DynamoDBMapper mapper = DynamoClient.getMapper();
		DynamoDBMapperConfig config = new DynamoDBMapperConfig(DynamoDBMapperConfig.ConsistentReads.CONSISTENT);
		RecordItem item1 = new RecordItem();
		item1.setId(sid);
		// Retrieve the item.
		DynamoDBQueryExpression<RecordItem> queryExpression = new DynamoDBQueryExpression<RecordItem>()
				.withHashKeyValues(item1);
		List<RecordItem> itemRetrieved = mapper.query(RecordItem.class, queryExpression, config);
		for(RecordItem r : itemRetrieved){
			mapper.delete(r);
		}
		return "No records exist";
	}
	
	@RequestMapping(value = "/university/{univ}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String deleteRecordByUniv(@PathVariable(value = "univ") String univ) {
		DynamoDBMapper mapper = DynamoClient.getMapper();
		DynamoDBMapperConfig config = new DynamoDBMapperConfig(DynamoDBMapperConfig.ConsistentReads.CONSISTENT);
		RecordItem item1 = new RecordItem();
		item1.setUniversity(univ);
		// Retrieve the item.
		DynamoDBQueryExpression<RecordItem> queryExpression = new DynamoDBQueryExpression<RecordItem>()
				.withHashKeyValues(item1);
		List<RecordItem> itemRetrieved = mapper.query(RecordItem.class, queryExpression, config);
		for(RecordItem r : itemRetrieved){
			mapper.delete(r);
		}
		return "No records exist";

	}
	
	private String convertListToString(List<RecordItem> records){
		if (records.size() > 0) {
			JSONArray array = new JSONArray();
			for (RecordItem r : records) {
				array.add(r.toJson());
			}
			return array.toJSONString();
		}
		return "No records exist";
	}
}